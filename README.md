
Bio3D Correlation Network Analysis
==================================

Bio3D-CNA is an [R](http://www.r-project.org/) package that facilitates network construction and community analysis of protein structure ensembles, e.g. obtained from molecular dynamics simulations.

It is a package of the larger [Bio3D](https://bitbucket.org/Grantlab/bio3d) family containing utilities for the analysis of protein structure, sequence and trajectory data. It is currently distributed as platform independent source code under the [GPL version 2 license](http://www.gnu.org/copyleft/gpl.html).

Features
--------

-   Network construction, community detection and refinement, suboptimal path analysis, and network visualization.
-   Building of both residue-based and community-based undirected weighted network graphs from an input correlation or contact map matrix, as obtained from the functions ‘cmap’, ‘dccm’, ‘dccm.nma’, and ‘dccm.enma’.
-   Community detection/clustering is performed on the initial residue based network to determine the community organization and network structure of the community based network.
-   Suboptimal path analysis is implemented to identify potentially key residues mediating allosteric coupling between distal sites.

Installation
------------

The development version of Bio3d-nma is available from BitBucket:

``` r
install.packages("devtools")
devtools::install_bitbucket("Grantlab/bio3d-cna")
```

Make sure you have installed [Bio3D](https://bitbucket.org/Grantlab/bio3d) core package prior to installing Bio3D-cna:

``` r
devtools::install_bitbucket("Grantlab/bio3d/bio3d-core", ref="core")
```

Basic usage for single structre analysis
----------------------------------------

``` r
# load library
library(bio3d.cna)
library(bio3d.nma)

pdb <- read.pdb("4Q21")
#>   Note: Accessing on-line PDB file
     
## Perform NMA
modes <- nma(pdb)
#>  Building Hessian...     Done in 0.017 seconds.
#>  Diagonalizing Hessian...    Done in 0.149 seconds.
     
## Calculate correlations 
cij <- dccm(modes)
#> 
  |                                                                            
  |====================================================                  |  74%
  |                                                                            
  |====================================================                  |  75%
  |                                                                            
  |=====================================================                 |  75%
  |                                                                            
  |=====================================================                 |  76%
  |                                                                            
  |======================================================                |  77%
  |                                                                            
  |======================================================                |  78%
  |                                                                            
  |=======================================================               |  78%
  |                                                                            
  |=======================================================               |  79%
  |                                                                            
  |========================================================              |  79%
  |                                                                            
  |========================================================              |  80%
  |                                                                            
  |========================================================              |  81%
  |                                                                            
  |=========================================================             |  81%
  |                                                                            
  |=========================================================             |  82%
  |                                                                            
  |==========================================================            |  82%
  |                                                                            
  |==========================================================            |  83%
  |                                                                            
  |===========================================================           |  84%
  |                                                                            
  |===========================================================           |  85%
  |                                                                            
  |============================================================          |  85%
  |                                                                            
  |============================================================          |  86%
  |                                                                            
  |=============================================================         |  86%
  |                                                                            
  |=============================================================         |  87%
  |                                                                            
  |=============================================================         |  88%
  |                                                                            
  |==============================================================        |  88%
  |                                                                            
  |==============================================================        |  89%
  |                                                                            
  |===============================================================       |  89%
  |                                                                            
  |===============================================================       |  90%
  |                                                                            
  |===============================================================       |  91%
  |                                                                            
  |================================================================      |  91%
  |                                                                            
  |================================================================      |  92%
  |                                                                            
  |=================================================================     |  92%
  |                                                                            
  |=================================================================     |  93%
  |                                                                            
  |=================================================================     |  94%
  |                                                                            
  |==================================================================    |  94%
  |                                                                            
  |==================================================================    |  95%
  |                                                                            
  |===================================================================   |  95%
  |                                                                            
  |===================================================================   |  96%
  |                                                                            
  |====================================================================  |  97%
  |                                                                            
  |====================================================================  |  98%
  |                                                                            
  |===================================================================== |  98%
  |                                                                            
  |===================================================================== |  99%
  |                                                                            
  |======================================================================|  99%
  |                                                                            
  |======================================================================| 100%
plot(cij, sse=pdb)
```

![](man/figures/README-syntax_demo-1.png)

``` r
     
## Build, and betweenness cluster, a network graph
net <- cna(cij, cutoff.cij=0.35)
#> Warning in (function (graph, weights = E(graph)$weight, directed = TRUE, :
#> At community.c:460 :Membership vector will be selected based on the lowest
#> modularity score.
#> Warning in (function (graph, weights = E(graph)$weight, directed = TRUE, : At
#> community.c:467 :Modularity calculation with weighted edge betweenness community
#> detection might not make sense -- modularity treats edge weights as similarities
#> while edge betwenness treats them as distances
plot(net, pdb)
#> Obtaining layout from PDB structure
```

![](man/figures/README-syntax_demo-2.png)

Citing Bio3D
------------

-   The Bio3D packages for structural bioinformatics <br> Grant, Skjærven, Yao, (2020) *Protein Science* <br> ( [Abstract](https://onlinelibrary.wiley.com/doi/abs/10.1002/pro.3923) | [PubMed]() | [PDF]() )

-   Integrating protein structural dynamics and evolutionary analysis with Bio3D. <br> Skjærven, Yao, Scarabelli, Grant, (2014) *BMC Bioinformatics* **15**, 399 <br> ( [Abstract](http://www.biomedcentral.com/1471-2105/15/399/abstract) | [PubMed](http://www.ncbi.nlm.nih.gov/pubmed/25491031) | [PDF](http://www.biomedcentral.com/content/pdf/s12859-014-0399-6.pdf) )

-   Bio3D: An R package for the comparative analysis of protein structures. <br> Grant, Rodrigues, ElSawy, McCammon, Caves, (2006) *Bioinformatics* **22**, 2695-2696 <br> ( [Abstract](http://bioinformatics.oxfordjournals.org/cgi/content/abstract/22/21/2695) | [PubMed](http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=retrieve&db=pubmed&list_uids=16940322&dopt=Abstract) | [PDF](http://bioinformatics.oxfordjournals.org/content/22/21/2695.full.pdf) )
