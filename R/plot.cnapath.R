#' @export
plot.cnapath <- function(x, ...) {
   .arg.filter <- utils::getFromNamespace(".arg.filter", "bio3d.core")
    
   arg.default <- list(plot=TRUE)
   args <- .arg.filter(arg.default, ...)
   do.call(print.cnapath, c(list(x=x), args))
}

#' @export
plot.ecnapath <- function(x, ...) {
   if(!inherits(x, "ecnapath")) {
      stop("The input 'x' must be an object of class 'ecnapath'.")
   }
   plot.cnapath(x, ...)
}
